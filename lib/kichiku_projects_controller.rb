require_dependency('projects_controller')
class ProjectsController < ApplicationController
  def index
    respond_to do |format|
      format.html {
        tmp_projects = Project.visible.find(:all, :order => 'name')
        @projects = Array.new
        sort_projects(tmp_projects)
      }
      format.api  {
        @offset, @limit = api_offset_and_limit
        @project_count = Project.visible.count
        @projects = Project.visible.all(:offset => @offset, :limit => @limit, :order => 'name')
      }
      format.atom {
        projects = Project.visible.find(:all, :order => 'created_on DESC',
                                              :limit => Setting.feeds_limit.to_i)
        render_feed(projects, :title => "#{Setting.app_title}: #{l(:label_project_latest)}")
      }
    end
  end

  private
  def sort_projects(tmp_projects)
    child_projects  = Array.new
    tmp_projects.each do |tp|
      if tp.parent_id.nil?
        @projects.push tp
      else
        child_projects.push tp
      end
    end
    tmp_projects = nil
    @projects.sort!      do |a,b| a.name <=> b.name end
    child_projects.sort! do |a,b| b.name <=> a.name end
    recursive_sort(child_projects)

  end

  def recursive_sort(child_projects)
    x = nil
    child_projects.each_with_index do |cp,i|
      break unless x.nil?
      @projects.each_with_index do |p,j|
        if p.id == cp.parent_id
          @projects.insert(j+1, cp)
          x = i
          break
        end
      end
    end
    child_projects.delete_at(x) unless x.nil?
    recursive_sort(child_projects) unless child_projects.empty?
  end
end
