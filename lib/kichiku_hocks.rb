module Kichiku
  class Hooks < Redmine::Hook::Listener
    def controller_issues_edit_before_save context
      issue = context[:issue]
      project = Project.find issue[:project_id].to_i
      issue_status_closed context
    end
    def view_issues_form_details_bottom(context)
<<SCRIPT
<script type="text/javascript">
function select_user() {
    var radio_elements = Form.getInputs('asform', 'radio', 'asssign_user');  
    var check_val = '';
    radio_elements.each(function (element)  
    {  
        if (element.checked == true) {  
            check_val = element.value;
            return;
        }  
    });
    var objSelect = $('issue_assigned_to_id');
    var len = objSelect.length;
    for(var i=0;i<len;i++) {
      if(objSelect.options[i].value == check_val){
        objSelect.options[i].selected = true;
      }else {
        objSelect.options[i].selected = false;
      }
    }
}
</script>
SCRIPT
    end

    private

    def issue_status_closed context
      issue = context[:issue]
      issue_status_closed = IssueStatus.find :all, :conditions => ["is_closed = (?)", true]

      issue_status_closed.each {|closed|
        if closed.id == issue[:status_id].to_i && issue[:done_ratio] != 100
          issue[:done_ratio] = 100
          context[:issue] = issue
        end if issue[:status_id] != nil && issue[:done_ratio] != nil
      } unless issue_status_closed.length == 0
    end

  end

end
