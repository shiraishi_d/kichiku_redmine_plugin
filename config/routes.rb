# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
match 'kichiku/search_asssign', :controller=> 'kichiku', :action => 'search_asssign', :via => :get
match 'kichiku/autocomplete_for_asssign_user', :controller=> 'kichiku', :action => 'autocomplete_for_asssign_user', :via => :get
match 'kichiku/show_files', :controller=> 'kichiku', :action => 'show_files', :via => :get
