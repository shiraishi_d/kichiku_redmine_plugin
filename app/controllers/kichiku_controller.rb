class KichikuController < ApplicationController
  before_filter :require_login, :check_project_privacy, :only => [:watch, :unwatch]

  def search_asssign
    @issue_id = params[:issue_id]
    @project_id = params[:project_id]
    @users = Project.find_by_identifier(@project_id).assignable_users
    respond_to do |format|
      format.js do
        render :update do |page|
          page.replace_html 'ajax-modal', :partial => 'kichiku/asssign', :locals => {:watched => @watched}
          page << "showModal('ajax-modal', '400px');"
          page << "$('ajax-modal').addClassName('asssign');"
        end
      end
    end
  end

  def autocomplete_for_asssign_user
    @users = User.active.like(params[:q]).find(:all, :limit => 100)
    @project_id = params[:project_id]
    as_users = Project.find_by_identifier(@project_id).assignable_users
    @users = @users & as_users
    render :layout => false
  end

  def show_files
    issue = Issue.find(params[:issue_id])
    @files = Hash.new
    changesets = issue.changesets.visible.all
    changesets.each do |changeset|
      repository = changeset.repository
      identifier = repository.identifier
      prefix_url   = repository.url.gsub(/#{repository.root_url}/,'')
      changes = changeset.filechanges.find(:all, :limit => 1000, :order => 'path').collect do |change|
        case change.action
        when 'A'
          # Detects moved/copied files
          if !change.from_path.blank?
            change.action =
              changeset.filechanges.detect {|c| c.action == 'D' && c.path == change.from_path} ? 'R' : 'C'
          end
          change
        when 'D'
          changeset.filechanges.detect {|c| c.from_path == change.path} ? nil : change
        else
          change
        end
      end.compact
      changes.each do |change|
        file = change.path.to_s.gsub(prefix_url,'')
        next unless file.include?('.')
        @files.store(identifier,Array.new) unless @files.key? identifier
        @files[identifier].push(file)
      end

    end
    @files.each do |key,value|
      @files[key] = value.sort.uniq
    end
    render :partial => 'kichiku/show_files'
  end

end
